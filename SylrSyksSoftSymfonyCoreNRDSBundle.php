<?php
/**
 * @file
 * Bundle.
 */
namespace SylrSyksSoftSymfony\CoreBundle\NRDS;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\NRDS
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
class SylrSyksSoftSymfonyCoreNRDSBundle extends Bundle {

}
