<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace SylrSyksSoftSymfony\CoreBundle\NRDS\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractDocument;
use SylrSyksSoftSymfony\CoreBundle\Model\TranslatableModelTrait;


/**
 * @MongoDB\MappedSuperclass()
 */
abstract class AbstractTranslatableDocument extends AbstractDocument implements Translatable
{
    use TranslatableModelTrait;
}