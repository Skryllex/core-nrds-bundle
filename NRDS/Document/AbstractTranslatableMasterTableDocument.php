<?php
/**
 * @file
 * Definition translatable master table.
 */
namespace SylrSyksSoftSymfony\CoreBundle\NRDS\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractMasterTableDocument;
use SylrSyksSoftSymfony\CoreBundle\Model\TranslatableModelTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Gedmo\Loggable()
 */
abstract class AbstractTranslatableMasterTableDocument extends AbstractMasterTableDocument implements Translatable
{
    use TranslatableModelTrait;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     * @Gedmo\Translatable()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     * @Gedmo\Translatable()
     */
    protected $description;
}