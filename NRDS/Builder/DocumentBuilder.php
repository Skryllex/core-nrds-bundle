<?php
/**
 * @file
 * Document builder.
 */
namespace SylrSyksSoftSymfony\CoreBundle\NRDS\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\AbstractObjectBuilder;

/**
 * Document builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\NRDS\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
class DocumentBuilder extends AbstractObjectBuilder
{

}