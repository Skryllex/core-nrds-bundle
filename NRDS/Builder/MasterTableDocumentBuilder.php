<?php
/**
 * @file
 * Master Table builder.
 */
namespace SylrSyksSoftSymfony\CoreBundle\NRDS\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\AbstractObjectBuilder;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderInterface;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderTrait;

/**
 * Master Table builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\NRDS\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class MasterTableDocumentBuilder extends AbstractObjectBuilder implements MasterTableBuilderInterface
{
    use MasterTableBuilderTrait;
}